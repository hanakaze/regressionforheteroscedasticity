

gradientDesc <- function(x,y,tolerance,maxIt){
   
    iteration = 1

    accuracy <- FALSE
    wts <- rep(1,length(x))
        
    while(!accuracy && iteration < maxIt){
        if(iteration %% 1000 ==0) print(paste('iteration ', iteration, '\n'))
        wtsOld <- wts
        model_temp<- lm(y~x, weight = wts)
        wts <- 1/(model_temp$residuals^2)
        accuracy <- (max(abs(wts - wtsOld)/wtsOld) < tolerance)
        
        iteration <- iteration + 1

    }
    if(iteration >= maxIt){ 
        print('not convergent')
        print(abs(wts - wtsOld)/wts)
    }
    return(list('iteration' = iteration,'model' = model_temp, 'wts' = wts))
}

