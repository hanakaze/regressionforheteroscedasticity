# README #

### Repo Purpose ###

Automaticly fits weighted least square regression using gradient descent for data sets with Heteroscedasticity

### Files ###

* data sets 1-5
* gradientDescent.R defines function for gradient descent
* knit solution.R to produce Rmarkdown to pdf report 
* run.R automates process for generating results for 5 data files.